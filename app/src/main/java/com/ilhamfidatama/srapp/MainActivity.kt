package com.ilhamfidatama.srapp

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ilhamfidatama.srapp.helper.OfflineHelper
import com.ilhamfidatama.srapp.internetService.InternetHelper
import com.ilhamfidatama.srapp.presenter.BottomSheetPresenter
import com.ilhamfidatama.srapp.presenter.CustomerPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (InternetHelper.online) {
            CustomerPresenter().loadAllCustomer()
        }

        val dataLocal = OfflineHelper.getLocalWork()
        val sizeDataLocal = if (dataLocal.isNotEmpty()){
             dataLocal.size
        }else{
            0
        }
        data_saved.text = resources.getString(R.string.amount_of_data).plus(sizeDataLocal)

        addDataWorker.setOnClickListener {
            val intent = Intent(this, AddData::class.java)
            startActivity(intent)
        }

        uploadData.setOnClickListener {
            if (sizeDataLocal != 0) {
                BottomSheetPresenter.showHorizontalProgress(this, dataLocal, supportFragmentManager)
            }else{
                BottomSheetPresenter.showNotification("Tidak Ada Data yang Tersimpan.", supportFragmentManager)
            }
        }

        btnLogout.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle("Logout")
                .setMessage("Apakah Anda yakin?")
                .setPositiveButton("Iya", logoutListener(true))
                .setNegativeButton("tidak", logoutListener(false))
                .show()
        }
    }

    private fun logoutListener(status: Boolean): DialogInterface.OnClickListener{
        return if (status){
            DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.dismiss()
                logout()
            }
        }else{
            DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.dismiss()
            }
        }
    }

    private fun logout(){
        OfflineHelper.deleteDataLogin()
        finishAffinity()
        startActivity(Intent(this, LoginActivity::class.java))
    }
}
