package com.ilhamfidatama.srapp.model

import com.orm.SugarRecord

class LoginPegawai(signIn: Boolean, idPegawai: String): SugarRecord() {
    var idPegawai: String = ""
    var signIn: Boolean = false

    init {
        this.idPegawai = idPegawai
        this.signIn = signIn
    }

    constructor(): this(false, "")
}