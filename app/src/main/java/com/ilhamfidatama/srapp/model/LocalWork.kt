package com.ilhamfidatama.srapp.model

import com.orm.SugarRecord
import java.io.File

class LocalWork(
    idPelanggan: Int, blnBongkar: String, merk: String, no: String, type: String, arus: String,
    blnPasang: String, kode: String, fotoSebelum: String, fotoSesudah: String, x: String,
    y: String, idPegawai: Int, region: Int, standBongkar: String): SugarRecord() {
    var id_pelanggan_modal: Int = 0
    var bln_thn_bongkar_modal: String = ""
    var merk_lama_modal: String = ""
    var no_lama_modal: String = ""
    var type_lama_modal: String = ""
    var arus_lama_modal: String = ""
    var bln_thn_pasang_modal: String = ""
    var kode_ket_modal: String = ""
    var foto_baru1: String = ""
    var foto_baru2: String = ""
    var ttk_koordinat_x_modal: String = ""
    var ttk_koordinat_y_modal: String = ""
    var user_modal: Int = 0
    var region_modal: Int = 0
    var stand_bongkar: String = ""

    init {
        id_pelanggan_modal = idPelanggan
        bln_thn_bongkar_modal = blnBongkar
        merk_lama_modal = merk
        no_lama_modal = no
        type_lama_modal = type
        arus_lama_modal = arus
        bln_thn_pasang_modal = blnPasang
        kode_ket_modal = kode
        foto_baru1 = fotoSebelum
        foto_baru2 = fotoSesudah
        ttk_koordinat_x_modal = x
        ttk_koordinat_y_modal = y
        user_modal = idPegawai
        region_modal = region
        stand_bongkar = standBongkar
    }

    constructor(): this(0, "", "", "", "", "", "", "", "", "", "", "", 0, 0, "")
}