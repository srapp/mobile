package com.ilhamfidatama.srapp.model

import android.os.Parcelable
import com.orm.SugarRecord
import kotlinx.android.parcel.Parcelize

class Location (longitude: Double, latitude: Double, accuracy: Float): SugarRecord(){
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    var accuracy: Float = 0.0F

    init {
        this.accuracy = accuracy
        this.latitude = latitude
        this.longitude = longitude
    }

    constructor(): this(0.0, 0.0, 0.0F)
}