package com.ilhamfidatama.srapp.model

import com.orm.SugarRecord

class LocalCustomer(id: String, nama: String, alamat: String, idRegion: String, idTabel: String): SugarRecord() {
    var idPelanggan: String = ""
    var namaPelanggan: String = ""
    var alamat: String = ""
    var idRegion: String = ""
    var idTabelPelanggan: String = ""

    init {
        this.idPelanggan = id
        this.namaPelanggan = nama
        this.alamat = alamat
        this.idRegion = idRegion
        this.idTabelPelanggan = idTabel
    }

    constructor(): this("", "", "", "", "")
}