package com.ilhamfidatama.srapp.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("status")
    @Expose
    val status: Boolean,
    @SerializedName("data")
    @Expose
    val data: T?
)