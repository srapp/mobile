package com.ilhamfidatama.srapp.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import com.ilhamfidatama.srapp.BuildConfig
import com.ilhamfidatama.srapp.api.services.CustomerServices
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Api2 {
    private val BASE_URL = BuildConfig.API_SRAPP_UPDATE
    private var gson = GsonBuilder().setLenient().create()
    private val httpClient by lazy {
        OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()
    }

    fun service(): CustomerServices{
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .client(httpClient)
            .build()
        return retrofit.create(CustomerServices::class.java)
    }
}