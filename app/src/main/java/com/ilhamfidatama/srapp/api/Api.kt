package com.ilhamfidatama.srapp.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import com.ilhamfidatama.srapp.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


object Api {
    private val BASE_URL: String = BuildConfig.API_SRAPP
    private var gson = GsonBuilder().setLenient().create()
    private val httpClient by lazy {
        OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()
    }
    fun <T>service(java: Class<T>): T{
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .client(httpClient)
            .build()
        return retrofit.create(java)
    }
}