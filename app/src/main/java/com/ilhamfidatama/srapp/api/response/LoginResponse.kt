package com.ilhamfidatama.srapp.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.ilhamfidatama.srapp.api.model.LoginModel

data class LoginResponse(
    @SerializedName("status")
    @Expose
    val status: Boolean,
    @SerializedName("token")
    @Expose
    val data: List<LoginModel>
)
