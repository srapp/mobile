package com.ilhamfidatama.srapp.api.model

data class LoginModel (
    var id_user: String,
    var fullname: String,
    var username: String,
    var password: String,
    var level: String,
    var foto: String,
    var id_region: String
)