package com.ilhamfidatama.srapp.api.services

import com.ilhamfidatama.srapp.api.model.LoginModel
import com.ilhamfidatama.srapp.api.response.BaseResponse
import com.ilhamfidatama.srapp.api.response.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {
    @POST("user/mlogin")
    fun doLogin(@Body login: LoginRequest): Call<LoginResponse>
}

data class LoginRequest(val username: String?, val password: String?, val level: String = "pegawai")