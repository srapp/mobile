package com.ilhamfidatama.srapp.api.model

data class CustomerModel (
    var id_pelanggan: String,
    var id_plg: String,
    var nama_pelanggan: String,
    var alamat: String,
    var id_region: String
)