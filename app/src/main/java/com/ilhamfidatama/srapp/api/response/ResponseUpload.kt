package com.ilhamfidatama.srapp.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseUpload (
    @SerializedName("status")
    @Expose
    val status: Boolean,
    @SerializedName("message")
    @Expose
    val message: String
)