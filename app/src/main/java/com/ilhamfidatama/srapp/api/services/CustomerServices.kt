package com.ilhamfidatama.srapp.api.services

import com.ilhamfidatama.srapp.api.response.BaseResponse
import com.ilhamfidatama.srapp.api.model.CustomerModel
import com.ilhamfidatama.srapp.api.response.ResponseUpload
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface CustomerServices {

    @GET("customer/mdatacustomer")
    fun getAllCustomer(): Call<BaseResponse<List<CustomerModel>>>

//    @GET("customer")
//    fun getCustomerbyID(@Header("Authorization") auth: String, @Query("id") id: String): Call<BaseResponse<CustomerModel>>

    @POST("client_customer/mdatacustomer")
    @Multipart
    fun uploadDataCutomer(
        @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part foto_baru1: MultipartBody.Part,
        @Part foto_baru2: MultipartBody.Part): Call<ResponseUpload>

}

