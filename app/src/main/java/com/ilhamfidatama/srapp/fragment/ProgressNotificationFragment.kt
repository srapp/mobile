package com.ilhamfidatama.srapp.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.LayoutInflater
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ilhamfidatama.srapp.R
import kotlinx.android.synthetic.main.buttomsheet_progress.view.*

class ProgressNotificationFragment(var message: String): BottomSheetDialogFragment() {
    @SuppressLint("InflateParams")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val layout = LayoutInflater.from(context).inflate(R.layout.buttomsheet_progress, null)
        layout.message_text.text = message
        dialog.setContentView(layout)
    }
}