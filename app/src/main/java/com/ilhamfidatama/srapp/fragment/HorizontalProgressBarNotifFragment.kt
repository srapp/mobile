package com.ilhamfidatama.srapp.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ilhamfidatama.srapp.R
import com.ilhamfidatama.srapp.api.Api2
import com.ilhamfidatama.srapp.api.response.ResponseUpload
import com.ilhamfidatama.srapp.helper.OfflineHelper
import com.ilhamfidatama.srapp.helper.UtilsHelper
import com.ilhamfidatama.srapp.model.LocalWork
import kotlinx.android.synthetic.main.bottomsheet_horizontal_progressbar.*
import retrofit2.Call
import retrofit2.Response


class HorizontalProgressBarNotifFragment(val dataLocal: MutableList<LocalWork>,
                                         val applicationContext: Context): BottomSheetDialogFragment() {
    private var uploaded = 0
    private var maxData = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottomsheet_horizontal_progressbar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val api = Api2.service()
        maxData = dataLocal.size
        Log.e("bs_upload", "$dataLocal = $maxData")
        progress_upload.max = maxData
        updateText()
        for (data in dataLocal){
            val (textBody, partBefore, partAfter) = UtilsHelper.convertData2RequestBody(applicationContext, data)
            val upload = api.uploadDataCutomer(textBody, partBefore, partAfter)
            uploadData(upload, data.id)
        }
    }

    private fun uploadData(upload: Call<ResponseUpload>, idData: Long){
        upload.enqueue(object : retrofit2.Callback<ResponseUpload>{
            override fun onFailure(call: Call<ResponseUpload>, t: Throwable) {
                Toast.makeText(applicationContext, "Gagal Upload. Periksa Kembali Koneksi Internet Anda", Toast.LENGTH_LONG).show()
                finishUpload()
            }

            override fun onResponse(
                call: Call<ResponseUpload>,
                response: Response<ResponseUpload>
            ) {
                response.body()?.let {
                    if (it.status){
                        uploaded += 1
                        Log.e("upload_success", "$uploaded")
                        updateText()
                        progress_upload.progress = uploaded
                        OfflineHelper.deleteLocalWorkByID(idData)
                    }
                    if (uploaded == maxData){
                        finishUpload()
                    }
                }
            }

        })
    }

    private fun finishUpload(){
        Handler().postDelayed({
            dismiss()
        }, 3000)
    }

    @SuppressLint("SetTextI18n")
    private fun updateText(){
        val upload = resources.getString(R.string.upload)
        if (uploaded != maxData){
            tittle_upload.text = "$upload $uploaded dari $maxData"
        }else {
            tittle_upload.text = "$upload Berhasil"
        }

    }

    override fun dismiss() {
        super.dismiss()
        val intent = activity?.intent
        intent?.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        activity?.finish()
        startActivity(intent)
    }
}