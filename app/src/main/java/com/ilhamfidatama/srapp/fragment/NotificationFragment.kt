package com.ilhamfidatama.srapp.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.LayoutInflater
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ilhamfidatama.srapp.R
import kotlinx.android.synthetic.main.buttomsheet_notification.view.*

class NotificationFragment(var message: String): BottomSheetDialogFragment() {
    @SuppressLint("InflateParams")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val layout = LayoutInflater.from(context).inflate(R.layout.buttomsheet_notification, null)
        layout.notif_text.text = message
        dialog.setContentView(layout)
    }
}