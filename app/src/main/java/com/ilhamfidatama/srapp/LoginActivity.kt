package com.ilhamfidatama.srapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ilhamfidatama.srapp.presenter.BottomSheetPresenter
import com.ilhamfidatama.srapp.presenter.LoginPresenter
import kotlinx.android.synthetic.main.activity_login.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener {
            signIn()
        }
    }

    private fun signIn(){
        val username = inputUsername.text.toString()
        val password = inputPassword.text.toString()
        val loginVM =ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(LoginPresenter::class.java)
        loginVM.doLogin(username, password, supportFragmentManager)
        loginVM.isSignIn().observe(this, Observer {
            if (it){
                goHomeActivity()
            }
        })
    }

    private fun goHomeActivity(){
        Handler().postDelayed({
            BottomSheetPresenter.endNotif()
            val home = Intent(this, MainActivity::class.java)
            finishAffinity()
            startActivity(home)
        }, 1500)
    }
}
