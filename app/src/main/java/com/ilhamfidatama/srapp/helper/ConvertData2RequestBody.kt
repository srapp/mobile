package com.ilhamfidatama.srapp.helper

import com.ilhamfidatama.srapp.model.LocalWork
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

object ConvertData2RequestBody {
    fun getTextBody(data: LocalWork): HashMap<String, RequestBody> {
        val map = HashMap<String, RequestBody>()
        map["id_pelanggan_modal"] = getRequestBodyText(data.id_pelanggan_modal.toString())
        map["bln_thn_bongkar_modal"] = getRequestBodyText(data.bln_thn_bongkar_modal)
        map["bln_thn_pasang_modal"] = getRequestBodyText(data.bln_thn_pasang_modal)
        map["ttk_koordinat_x_modal"] = getRequestBodyText(data.ttk_koordinat_x_modal)
        map["ttk_koordinat_y_modal"] = getRequestBodyText(data.ttk_koordinat_y_modal)
        map["no_lama_modal"] = getRequestBodyText(data.no_lama_modal)
        map["arus_lama_modal"] = getRequestBodyText(data.arus_lama_modal)
        map["type_lama_modal"] = getRequestBodyText(data.type_lama_modal)
        map["merk_lama_modal"] = getRequestBodyText(data.merk_lama_modal)
        map["user_modal"] = getRequestBodyText(data.user_modal.toString())
        map["region_modal"] = getRequestBodyText(data.region_modal.toString())
        map["kode_ket_modal"] = getRequestBodyText(data.kode_ket_modal)
        map["stand_bongkar_modal"] = getRequestBodyText(data.stand_bongkar)
        return map
    }

    fun getRequestBodyText(value: String) = RequestBody.create(
        MediaType.parse("text/plain"),
        value)

    fun getPartBodyImage(imageFile: File, name: String): MultipartBody.Part {
        val requestBody = RequestBody.create(MediaType.parse("image.jpg"), imageFile)
        return MultipartBody.Part.createFormData(name, imageFile.name, requestBody)
    }
}