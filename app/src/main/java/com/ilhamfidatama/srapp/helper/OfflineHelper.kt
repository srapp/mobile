package com.ilhamfidatama.srapp.helper

import com.ilhamfidatama.srapp.model.LocalCustomer
import com.ilhamfidatama.srapp.model.LocalWork
import com.ilhamfidatama.srapp.model.Location
import com.ilhamfidatama.srapp.model.LoginPegawai
import com.orm.SugarRecord
import java.io.File

object OfflineHelper {
    fun addDataLogin(signIn: Boolean, idPegawai: String){
        val login = LoginPegawai(signIn, idPegawai)
        login.save()
    }

    fun getDataLogin() = SugarRecord.listAll(LoginPegawai::class.java).getOrNull(0)

    fun isSignIn() = getDataLogin()

    fun deleteDataLogin(){
        SugarRecord.deleteAll(LoginPegawai::class.java)
    }

    fun addDataCustomer(id: String, nama: String, alamat: String, idRegion: String, idTabel: String){
        val customer = LocalCustomer(id, nama, alamat, idRegion, idTabel)
        customer.save()
    }

    fun deleteCustomer(){
        SugarRecord.deleteAll(LocalCustomer::class.java)
    }

    fun getDataCustomer() = SugarRecord.listAll(LocalCustomer::class.java)

    fun findCustomer(id: String) = getDataCustomer().find { it.idPelanggan == id }

    fun addLocation(accuracy: Float, longitude: Double, latitude: Double){
        deleteLocation()
        val location = Location(longitude, latitude, accuracy)
        location.save()
    }

    fun deleteLocation(){
        SugarRecord.deleteAll(Location::class.java)
    }

    fun getLocation() = SugarRecord.listAll(Location::class.java).getOrNull(0)

    fun addLocalWork(localWork: LocalWork){
        localWork.save()
    }

    fun deleteLocalWork() {
        SugarRecord.deleteAll(LocalWork::class.java)
    }

    fun getLocalWork() = SugarRecord.listAll(LocalWork::class.java)

    fun deleteLocalWorkByID(id: Long){
        val localWork = SugarRecord.findById(LocalWork::class.java, id)
        localWork.delete()
    }
}