package com.ilhamfidatama.srapp.helper

import android.content.Context
import com.ilhamfidatama.srapp.model.LocalWork
import id.zelory.compressor.Compressor
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

object UtilsHelper {

    fun convertData2RequestBody(context: Context, data: LocalWork): Triple<HashMap<String, RequestBody>,
            MultipartBody.Part,
            MultipartBody.Part>{
        val textBody = ConvertData2RequestBody.getTextBody(data)
        val imageFileBefore = compressingImage(context, data.foto_baru1)
        val imageFileAfter = compressingImage(context, data.foto_baru2)
        val partBefore = ConvertData2RequestBody.getPartBodyImage(imageFileBefore,"foto_baru1")
        val partAfter = ConvertData2RequestBody.getPartBodyImage(imageFileAfter, "foto_baru2")
        return Triple(textBody, partBefore, partAfter)
    }

    fun compressingImage(context: Context, imagePath: String): File{
        val imageFile = File(imagePath)
        return Compressor(context).compressToFile(imageFile)
    }

}