package com.ilhamfidatama.srapp

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import com.github.dhaval2404.imagepicker.ImagePicker
import com.ilhamfidatama.srapp.api.Api2
import com.ilhamfidatama.srapp.api.response.ResponseUpload
import com.ilhamfidatama.srapp.helper.OfflineHelper
import com.ilhamfidatama.srapp.helper.UtilsHelper
import com.ilhamfidatama.srapp.internetService.InternetHelper
import com.ilhamfidatama.srapp.model.LocalWork
import com.ilhamfidatama.srapp.presenter.BottomSheetPresenter
import kotlinx.android.synthetic.main.activity_form_worker.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class FormWorkerActivity : AppCompatActivity() {

    companion object{
        const val TEMP = ""
    }
    private var imagePathSebelum: String? = null
    private var imagePathSesudah: String? = null
    private lateinit var pekerjaan: String
    private lateinit var listKode: Array<String>
    private lateinit var merk: String
    private lateinit var tipe: String
    private lateinit var arus: String
    private lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_worker)

        val location = intent.getParcelableExtra(TEMP) as TempLocation

        val idPelanggan = intent.getStringExtra("id-pelanggan") as String
        val region = intent.getStringExtra("region") as String
        listKode = resources.getStringArray(R.array.kode_meteran)
        val listMerk = resources.getStringArray(R.array.merk_meteran)
        val listArus = resources.getStringArray(R.array.arus_meteran)
        val listTipe = resources.getStringArray(R.array.type_meteran)
        val listPekerjaan = resources.getStringArray(R.array.pekerjaan)
        inputNoMeteran.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                checkMeteran(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
        inputMerk.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                merk = if (p2 == 0) ""
                else listMerk[p2]
            }
        }
        inputTipe.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                tipe = if (p2 == 0) ""
                else listTipe[p2]
            }
        }
        inputArus.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                arus = if (p2 == 0) ""
                else listArus[p2]
            }
        }

        inputPekerjaan.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                pekerjaan = if (p2 == 0) ""
                else listPekerjaan[p2]
            }

        }

        btnTakeImageBefore.setOnClickListener {
            imageView = findViewById(R.id.imageBefore)
            ImagePicker.with(this)
                .compress(500)
                .start { resultCode, dataIntent ->
                    if (resultCode == Activity.RESULT_OK){
                        val imageUri = dataIntent?.data
                        imageView.setImageURI(imageUri)
                        imagePathSebelum = ImagePicker.getFilePath(dataIntent)
                    }
                }
        }

        btnTakeImageAfter.setOnClickListener {
            imageView = findViewById(R.id.imageAfter)
            ImagePicker.with(this)
                .compress(500)
                .start { resultCode, data ->
                    if (resultCode == Activity.RESULT_OK){
                        val imageUri = data?.data
                        imageView.setImageURI(imageUri)
                        imagePathSesudah = ImagePicker.getFilePath(data)
                    }
                }
        }

        btnAddResult.setOnClickListener {
            val inputStandBongkar = input_stand_bongkar.text.toString()
            if (verificationData() && inputStandBongkar.isNotEmpty()) {
                val noMeteran = inputNoMeteran.text.toString()
                saveData(idPelanggan, location.latitude, location.longitude, noMeteran, region, inputStandBongkar)
            }else{
                BottomSheetPresenter.showNotification("Lengkapi Formulir Anda", supportFragmentManager)
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun saveData(customerID: String, x: Double, y: Double, idMeteran: String, region: String, standBongkar: String){
        val date = SimpleDateFormat("dd/MM/yyyy").format(Date())
        var idPegawai = 0
        OfflineHelper.getDataLogin()?.idPegawai?.toInt()?.let {
            idPegawai = it
        }
        val localWork = LocalWork(customerID.toInt(), date, merk, idMeteran, tipe, arus, date, pekerjaan,
            imagePathSebelum.toString(), imagePathSesudah.toString(), x.toString(), y.toString(), idPegawai, region.toInt(), standBongkar)
        if (InternetHelper.online){
            BottomSheetPresenter.showProgress("Proses Upload...", supportFragmentManager)
            val (body, partImageBefore, partImageAfter) = UtilsHelper.convertData2RequestBody(this, localWork)
            upload(body, partImageBefore, partImageAfter)
        }else{
            OfflineHelper.addLocalWork(localWork)
            BottomSheetPresenter.showNotification("Data Berhasil Disimpan.", supportFragmentManager)
            uploadSuccess()
        }
    }

    private fun checkMeteran(id: String){
        when {
            id.length == 2 -> {
                val result = listKode.indexOf(id)
                showDataMeteran(result+1)
            }
            id.isEmpty() -> {
                showDataMeteran(0)
            }
        }
    }

    private fun showDataMeteran(index: Int){
        inputMerk.setSelection(index)
        inputArus.setSelection(index)
        inputTipe.setSelection(index)
    }

    private fun upload(
        body: Map<String, RequestBody>,
        imageBefore: MultipartBody.Part,
        imageAfter: MultipartBody.Part){
        val service = Api2.service().uploadDataCutomer(body, imageBefore, imageAfter)
        service.enqueue(object: Callback<ResponseUpload>{
            override fun onFailure(call: Call<ResponseUpload>, t: Throwable) {
                Log.e("upload-failure", "${t.message}")
                BottomSheetPresenter.endProgress()
                BottomSheetPresenter.showNotification(
                    "Gagal Upload. Cek Koneksi Internet Anda.",
                    supportFragmentManager)
            }

            override fun onResponse(
                call: Call<ResponseUpload>,
                response: Response<ResponseUpload>
            ) {
                Log.w("upload-response", "${response.body()}")
                if (response.isSuccessful){
                    BottomSheetPresenter.endProgress()
                    response.body()?.status?.let { uploadResponse(it) }
                }
            }
        })
    }

    private fun uploadSuccess(){
        Handler().postDelayed({
            BottomSheetPresenter.endNotif()
            val intent = Intent(this@FormWorkerActivity, MainActivity::class.java)
            finishAffinity()
            startActivity(intent)
        }, 2000)
    }

    private fun uploadResponse(status: Boolean){
        if (status){
            BottomSheetPresenter.showNotification("Upload Berhasil.", supportFragmentManager)
            uploadSuccess()
        }else{
            BottomSheetPresenter.showNotification("Upload Gagal.", supportFragmentManager)
        }
    }

    private fun verificationData(): Boolean = merk.isNotEmpty() &&
            tipe.isNotEmpty() &&
            arus.isNotEmpty() &&
            pekerjaan.isNotEmpty() &&
            !imagePathSebelum.isNullOrEmpty() &&
            !imagePathSesudah.isNullOrEmpty()

}