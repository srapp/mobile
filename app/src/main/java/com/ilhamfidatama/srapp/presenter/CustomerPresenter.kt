package com.ilhamfidatama.srapp.presenter

import android.util.Log
import com.ilhamfidatama.srapp.api.Api
import com.ilhamfidatama.srapp.api.response.BaseResponse
import com.ilhamfidatama.srapp.api.model.CustomerModel
import com.ilhamfidatama.srapp.api.services.CustomerServices
import com.ilhamfidatama.srapp.helper.OfflineHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CustomerPresenter {

    fun loadAllCustomer(){
        val apiCustomer = Api.service(CustomerServices::class.java)
        apiCustomer.getAllCustomer()
            .enqueue(object : Callback<BaseResponse<List<CustomerModel>>>{
                override fun onFailure(call: Call<BaseResponse<List<CustomerModel>>>, t: Throwable) {
                    Log.e("failure", "${t.message}")
                }

                override fun onResponse(
                    call: Call<BaseResponse<List<CustomerModel>>>,
                    response: Response<BaseResponse<List<CustomerModel>>>
                ) {
                    saveCustomers(response.body())
                }
            })
    }

    private fun saveCustomers(body: BaseResponse<List<CustomerModel>>?){
        val customers = body?.data
        OfflineHelper.deleteCustomer()
        if (customers != null) {
            Thread(Runnable {
                for (customer in customers){
                    OfflineHelper.addDataCustomer(customer.id_plg, customer.nama_pelanggan,
                        customer.alamat, customer.id_region, customer.id_pelanggan)
                }
            }).start()
        }
    }
}