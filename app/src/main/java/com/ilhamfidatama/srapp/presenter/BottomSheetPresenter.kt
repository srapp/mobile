package com.ilhamfidatama.srapp.presenter

import android.content.Context
import android.os.Handler
import androidx.fragment.app.FragmentManager
import com.ilhamfidatama.srapp.fragment.HorizontalProgressBarNotifFragment
import com.ilhamfidatama.srapp.fragment.NotificationFragment
import com.ilhamfidatama.srapp.fragment.ProgressNotificationFragment
import com.ilhamfidatama.srapp.model.LocalWork

object BottomSheetPresenter {
    private lateinit var progress: ProgressNotificationFragment
    private lateinit var notif: NotificationFragment
    private lateinit var horizontalProgress: HorizontalProgressBarNotifFragment
    fun showNotification(message: String, fragment: FragmentManager){
        notif = NotificationFragment(message)
        notif.show(fragment, "notif")
    }

    fun showProgress(message: String, fragment: FragmentManager){
        progress = ProgressNotificationFragment(message)
        progress.show(fragment, "progress")
    }

    fun showHorizontalProgress(context: Context, allData: MutableList<LocalWork>, fragment: FragmentManager){
        horizontalProgress = HorizontalProgressBarNotifFragment(allData, context)
        horizontalProgress.show(fragment, "upload-data-local")
        horizontalProgress.isCancelable = false
    }

    fun endProgress(){
        progress.dismiss()
    }

    fun endNotif(){
        notif.dismiss()
    }
}