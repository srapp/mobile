package com.ilhamfidatama.srapp.presenter

import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ilhamfidatama.srapp.api.Api
import com.ilhamfidatama.srapp.api.response.LoginResponse
import com.ilhamfidatama.srapp.api.services.LoginRequest
import com.ilhamfidatama.srapp.api.services.LoginService
import com.ilhamfidatama.srapp.helper.OfflineHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter: ViewModel() {

    private var signIn = MutableLiveData<Boolean>()
    private var api = Api.service(LoginService::class.java)

    fun doLogin(username: String, password: String, fragmentManager: FragmentManager){
        BottomSheetPresenter.showProgress("Sign In", fragmentManager)
        val request = LoginRequest(username, password)
        api.doLogin(request).enqueue(object : Callback<LoginResponse>{
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.e("failure", "${t.message}")
                BottomSheetPresenter.endProgress()
                BottomSheetPresenter.showNotification("Login Gagal. Periksa Ulang Username dan Password atau Koneksi Internet Anda.", fragmentManager)
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {
                Log.w("loginResponse", "${response.body()}")
                if (response.isSuccessful && isBodyNotNull(response.body())){
                    savetoLocal(response.body())
                    BottomSheetPresenter.endProgress()
                    BottomSheetPresenter.showNotification("Login Berhasil.", fragmentManager)
                }else{
                    BottomSheetPresenter.showNotification("Login Gagal.", fragmentManager)
                }
            }

        })
    }

    private fun savetoLocal(data: LoginResponse?){
        val loginData = data?.data?.firstOrNull()
        loginData?.let {
            OfflineHelper.addDataLogin(true, it.id_user)
            signIn.postValue(true)
        }
    }

    fun isSignIn() = signIn

    fun isBodyNotNull(data: LoginResponse?): Boolean{
        var body = false
        data?.let {
            body = it.status
        }
        return body
    }
}