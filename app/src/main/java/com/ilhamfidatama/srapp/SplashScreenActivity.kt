package com.ilhamfidatama.srapp

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.facebook.stetho.Stetho
import com.google.android.gms.location.*
import com.ilhamfidatama.srapp.helper.OfflineHelper
import com.ilhamfidatama.srapp.internetService.InternetHelper
import com.orm.SugarContext
import com.orm.SugarDb

class SplashScreenActivity : AppCompatActivity() {
    companion object{
        private const val REQUEST_PERMISSION_REQUEST_CODE = 1
    }

    private var locationRequest: LocationRequest? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val UPDATE_INTERVAL:Long = 5
    private val FASTEST_INTERVAL:Long = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        SugarContext.init(applicationContext)
        val databaseLocal = SugarDb(this)
        databaseLocal.onCreate(databaseLocal.db)

        Stetho.initializeWithDefaults(this)

        InternetHelper.internetService(applicationContext)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        startLocationUpdate()
    }

    private fun isSignIn(): Boolean{
        val login = OfflineHelper.isSignIn()
        return login != null
    }

    private fun startLocationUpdate(){
        locationRequest = LocationRequest.create()
        locationRequest!!.run {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = UPDATE_INTERVAL
            fastestInterval = FASTEST_INTERVAL
        }

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest!!)
        val locationSettingRequest = builder.build()

        val settingClient = LocationServices.getSettingsClient(this)
        settingClient?.checkLocationSettings(locationSettingRequest)

        getLocation()
    }

    private fun getLocation(){
        val locationCallBack = object : LocationCallback() {
            override fun onLocationResult(loc: LocationResult?) {
                val location = loc?.lastLocation
                Log.e("location", "akurasi:${location?.accuracy}, long:${location?.longitude}, lat:${location?.latitude}")
                location?.let {
                    if (it.accuracy < 30){
                        OfflineHelper.addLocation(it.accuracy, it.longitude, it.latitude)
                    }
                }
            }
        }
        if (Build.VERSION.SDK_INT >= 23 && checkPermission()){
            val fusedLocation = LocationServices.getFusedLocationProviderClient(this)
            fusedLocation.requestLocationUpdates(locationRequest, locationCallBack, Looper.getMainLooper())
            moveActivity()
        }
    }

    private fun checkPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            true
        }else{
            requestPermission()
            false
        }
    }

    private fun requestPermission(){
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            REQUEST_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getLocation()
                    moveActivity()
                }
            }
        }
    }

    private fun moveActivity(){
        val signIn = isSignIn()
        Handler().postDelayed({
            val intent: Intent = if (signIn){
                Intent(this@SplashScreenActivity, MainActivity::class.java)
            }else{
                Intent(this@SplashScreenActivity, LoginActivity::class.java)
            }
            finishAffinity()
            startActivity(intent)
        }, 3000)
    }
}
