package com.ilhamfidatama.srapp.internetService

import android.content.Context

object InternetHelper {
    var online = false

    fun internetService(context: Context){
        ConnectionService(context).startInternetService()
    }
}