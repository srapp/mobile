package com.ilhamfidatama.srapp.internetService

import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log

class ConnectionService(private val context: Context): Service(), ServiceListener {
    override fun onCreate() {
        runInternetService()
    }

    override fun onBind(p0: Intent?): IBinder? {
        runInternetService()
        return null
    }

    fun startInternetService(){
        InternetHelper.online = getConnectivityStatus()
        runInternetService()
    }

    fun runInternetService(){
        Handler().postDelayed({
            InternetHelper.online = getConnectivityStatus()
            Log.e("internet", "${InternetHelper.online}")
            runInternetService()
        }, 3000)
    }

    fun getConnectivityStatus(): Boolean{
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            val active = cm.activeNetwork
            if (active != null){
                val networkCapability = cm.getNetworkCapabilities(active)
                return (networkCapability.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || networkCapability.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
            }
        }else{
            val active = cm.activeNetworkInfo
            if (active != null){
                return ((active.type == ConnectivityManager.TYPE_MOBILE) || (active.type == ConnectivityManager.TYPE_WIFI))
            }
        }
        return false
    }

    override fun update() {
        onCreate()
    }
}

interface ServiceListener{
    fun update()
}