package com.ilhamfidatama.srapp

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.widget.Toast
import com.ilhamfidatama.srapp.helper.OfflineHelper
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_add_data.*

class AddData : AppCompatActivity(){

    private var accuracy: Float = 100.0F
    private lateinit var idCustomer: String
    private lateinit var tempLocation: TempLocation
    private lateinit var idRegion: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_data)
        showAccuracy()
        OfflineHelper.deleteLocation()

        btnLanjut.setOnClickListener {
            toFormWorker()
        }

        btnSearch.setOnClickListener {
            val id = input_id_pelanggan.text.toString()
            Log.w("search", id)
            searchCustomer(id)
        }
    }

    private fun searchCustomer(id: String) {
        val result = OfflineHelper.findCustomer(id)
        var found = false
        result?.let {
            idCustomer = it.idTabelPelanggan
            idRegion = it.idRegion
            showCustomerData(it.namaPelanggan, it.alamat)
            found = true
        }
        showLayoutCustomer(found)
    }

    private fun showCustomerData(nama: String, alamat: String){
        nama_pelanggan.text = nama
        alamat_pelanggan.text = alamat
    }

    private fun showLayoutCustomer(found: Boolean){
        if (found){
            customerDetail.visibility = View.VISIBLE
        }else{
            idCustomer = ""
            idRegion = ""
            customerDetail.visibility = View.GONE
        }
    }

    private fun toFormWorker(){
        val intent = Intent(this, FormWorkerActivity::class.java)
        if (this.accuracy <= 30.0F && idCustomer.isNotEmpty()){ // && customerID.isNotEmpty()
            intent.putExtra("id-pelanggan", idCustomer)
            intent.putExtra("region", idRegion)
            intent.putExtra(FormWorkerActivity.TEMP, tempLocation)
            startActivity(intent)
        }else{
            Toast.makeText(this, "GPS Belum Akurat", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showAccuracy(){
        Handler().postDelayed({
            var location = OfflineHelper.getLocation()
            var accurate = resources.getString(R.string.akurasi)
            if (location != null){
                accurate = "Akurasi : +-${location.accuracy} Meter"
                tempLocation = TempLocation(location.longitude, location.latitude)
                accuracy = location.accuracy
            }
            accuracy_text.text = accurate
            showAccuracy()
        }, 3000)
    }
}

@Parcelize
data class TempLocation(
    var longitude: Double,
    var latitude: Double
) : Parcelable